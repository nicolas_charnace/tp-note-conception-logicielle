# Lancement du webservice

Commande pour lancer rapidement le webservice

```
cd webservice
uvicorn main:app --reload
```
Pour les dépendances voir requirements.txt
```
pip install -r requirements.txt
```

# Description du webservice

Récupère un deck de l'api https://deckofcardsapi.com/ via son deck_id
Tire des cartes de ce deck et renvoie la liste de ces cartes de la forme :
```
{
"image": "https://deckofcardsapi.com/static/img/8C.png",
"value": "8",
"suit": "CLUBS",
"code": "8C"
}
```

Webservice conçu à partir de FastApi https://fastapi.tiangolo.com/
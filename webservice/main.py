from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
import requests

class Deck (BaseModel):
    deck_id : str

app = FastAPI()

@app.get("/creer-un-deck")
async def get_deck():
    deck_shuffled = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    deck_id = deck_shuffled.json()["deck_id"]
    return deck_id

@app.post("/cartes/{nombre_cartes}")
async def tirer_cartes(nombre_cartes : int, deck : Deck):
    tirer_carte_deck = requests.get("https://deckofcardsapi.com/api/deck/"
                + deck.deck_id
                + "/draw/?count="
                + str(nombre_cartes)
            )
    cartes = tirer_carte_deck.json()["cards"]
    return cartes

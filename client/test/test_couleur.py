import pytest
import requests

def couleur(liste_cartes):
    dict_couleur = {"H":0,"S":0,"D":0,"C":0}
    for carte in liste_cartes:
        dict_couleur[carte["code"][1]] += 1
    return dict_couleur

def test_couleur():
    dict_attendu = {"H":0,"S":1,"D":1,"C":2}
    deck_id = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?cards=KC,AS,2C,QD").json()["deck_id"]
    liste_cartes = requests.get("https://deckofcardsapi.com/api/deck/" + deck_id + "/draw/?count=4").json()["cards"]
    assert couleur(liste_cartes) == dict_attendu
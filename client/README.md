# Lancement du client

Le webservice doit être lancé au préalable.
Commande pour lancer rapidement le client
```
cd client
python main.py
```
Pour les dépendances voir requirements.txt
```
pip install -r requirements.txt
```

# Lancement des tests

```
pytest
```
# Description de l'executable

 Initialise un deck, 
 Tire 10 cartes,
Compte le nombre de cartes tirées de chaque couleur
import requests
import json

class Deck:
    deck_id : str
    def __init__(self, deck_id):
        self.deck_id = deck_id


def couleur(liste_cartes):
    dict_couleur = {"H":0,"S":0,"D":0,"C":0}
    for carte in liste_cartes:
        dict_couleur[carte["code"][1]] += 1
    return dict_couleur

if __name__ == "__main__":
    deck_id = requests.get("http://127.0.0.1:8000/creer-un-deck").json()
    deck = Deck(deck_id = deck_id)
    liste_cartes = requests.post("http://127.0.0.1:8000/cartes/10", json = deck.__dict__).json()
    print(couleur(liste_cartes))
